---
title: Introduction
type: docs
title: "Product Documentation"
---

# Product Documentation

Welcome to Entgra Produt Documentation!

The latest version of Entgra IoT Server that has been released is <a tareget="_blank" href="docs/about-entgra-IoT-server/about-this-release/">4.0.0</a>. In this section, you will find all related documentation pertaining to our product.

Our Product Guide provides comprehensive navigation on working with the product, from installation to device management covering vital areas like device enrollment for all types of devices.

Key Concepts aims to briefly take you through the commonly used terminology and their context in relation to our product. You can refer to this section for any usages that you are not familiar with, and/or to gain in-depth descriptive information about specific topics when using the product.

Click on any of the following sections relevant to your area of interest to get started:

<table border="1" class="tableizer-table" width="100%">
<thead bgcolor="#B8C4CC">
<tr class="tableizer-firstrow"><th><font style="Tahoma" size="2">About Entgra IoT Server</font></th>
<th><font style="Arial" size="2">Product Guide</font></th>
<th><font style="Arial" size="2">Key Concepts</font></th>
</tr></thead>
<tbody aria-autocomplete="both">
<tr><td align="center" width="25%"><font style="Tahoma" size="2">
<div>
    <ul>
        <li><a href="docs/about-entgra-IoT-server/">Entgra IoT Server</a></li>
        <li><a href="docs/about-entgra-IoT-server/architecture/">Architecture</a></li>
        <li><a href="docs/about-entgra-IoT-server/about-this-release/">About This Release</a></li>
        <li><a href="docs/about-entgra-IoT-server/feature-list/">Feature List</a></li>
        <li><a href="docs/about-entgra-IoT-server/release-archive/">Release Archive</a></li>
    </ul>
</div>
</font></td>
<td align="center" width="35%"><font style="Tahoma" size="2">
<div>
    <ul>
        <li><a href="docs/product-guide/system-requirements/">System Requirements</a></li>
        <li><a href="docs/product-guide/download-and-start-the-server/">Download and Start the Server</a></li>
        <li><a href="docs/product-guide/login-guide/">Log-In Guide</a></li>
        <li><a href="docs/product-guide/enrollment-guide/">Enrollment Guide</a></li>
        <li><a href="docs/product-guide/device-management-guide/">Device Management Guide</a></li>
        <li><a href="docs/product-guide/manage-tenants/">Manage Tenants</a></li>
        <li><a href="docs/product-guide/manage-roles/">Manage Roles</a></li>
        <li><a href="docs/product-guide/manage-users/">Manage Users</a></li>
        <li><a href="docs/product-guide/app-management/">App Management</a></li>
        </ul>
    </div>
</td>
<td align="center" width="35%"><font style="Tahoma" size="2">
<div>
    <ul>
        <li><a href="docs/key-concepts/devices/">Devices</a></li>
        <li><a href="docs/key-concepts/enrollment/">Enrollment</a></li>
        <li><a href="docs/key-concepts/mobile-applications/">Mobile Applications</a></li>
        <li><a href="docs/key-concepts/operations-and-policies/">Operations and Policies</a></li>
        <li><a href="docs/key-concepts/user-management/">User Management</a></li>
        <li><a href="docs/key-concepts/api/">APIs</a></li>
        <li><a href="docs/key-concepts/extensions/">Extensions</a></li>
        <li><a href="docs/key-concepts/security/">Security</a></li>
        <li><a href="docs/key-concepts/databases/">Databases</a></li>
        </ul>
        </div>
        </tr>
</tbody></table>


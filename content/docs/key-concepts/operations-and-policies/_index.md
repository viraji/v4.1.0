---
bookCollapseSection: true
weight: 5
---

# Operations

Each device supports a set of operations depending on its platform (i.e., Android, iOS, Windows),  such as screen lock, device unlock, and device reboot. Entgra IoT Server facilitates these operations to be performed remotely via the **Device Management Console**.

Operations can be performed by the following roles: 

<ul style="list-style-type:disc;">

<li>  <strong>Device Admin</strong>: Users with this role can perform operations on multiple devices that are under their control.</li>
<li>  <strong>Owner</strong>: Users with this role can perform operations on their own devices.</li>
</ul>

To view the list of operations supported for the Android, iOS, and Windows devices, 
see Supported Operations for Mobile Devices for <a href="../../about-entgra-IoT-server/feature-list/android/">Anroid</a>, <a href="../../about-entgra-IoT-server/feature-list/ios/">iOS</a> and <a href="../../about-entgra-IoT-server/feature-list/windows/">Windows</a>.</li>


# Policies

A policy is a set of configurations enforced on a device, that influences the device functionality. Policies are able to control the settings on devices, inform the user when the device is not responding as expected and much more. For example, you can disable the camera on a mobile device via a policy.

Policies can be created and applied to devices by the following user roles:

<ul style="list-style-type:disc;">
<li>  <strong>Device Admin</strong>: Users with this role can create and enforce policies on multiple devices, under their control, and monitor policy compliance. This behavior is more relevant to mobile device admins in a corporate environment.</li>

<li>  <strong>Device Owner</strong> Users with this role can create and enforce policies on their own devices. This behavior is more relevant to IoT device owners, but depending on your organizational policies and procedures it can apply to mobile device owners as well.</li>
</ul>

In Entgra IoT Server, a collection of policies is called a **profile**. Policy profiles allow you
 to apply multiple policies to a device collectively. Entgra IoT Server has predefined policies 
 for <a href="../../about-entgra-IoT-server/feature-list/android/">Anroid</a>, <a href="../../about-entgra-IoT-server/feature-list/ios/">iOS</a> and <a href="../../about-entgra-IoT-server/feature-list/windows/">Windows</a> in place to manage mobile devices 
 and supports creating custom 
 policies for IoT devices.

Let's take a look at how a policy is enforced on a device:
<ul style="list-style-type:disc;">
<li>  <strong>Step 1: Filtering based on the Platform (device type)</strong> Policies are filtered based on the mobile platform to match each policy with the platform of the registered device.</li>
<li>  <strong>Step 2: Filtering based on the device ownership type</strong> Next, the policies are filtered based on the device ownership type (i.e., `BYOD` or `COPE`) to match with the device ownership type of the registered device.</li>
<li>  <strong>Step 3: Filtering based on the user role or name</strong>The policies are filtered again to match the device owners username or role.</li>
<li>  <strong>Step 4: Enforcing the policy</strong> Finally, the policy having the highest priority out of the pool of filtered policies is enforced on the registered device. </li>
</ul>

For more information on creating and applying policies, see [Android]({{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/) and 
[iOS]({{< param doclink >}}product-guide/device-management-guide/apple-devices/apple-device-policies/).


---
bookCollapseSection: true
weight: 1
---


# Publishing an App and Adding Reviews

<br><iframe width="560" height="315" src="https://www.youtube.com/embed/FnNi5ZMEdYU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<strong><h2>How to Publish an App</h2></strong>

<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Go to the Publisher portal.</li>
    <li>On the navigation bar, select the application type from <b>Add New App</b> drop-down .</li>
    <li>Fill the forms respectively.</li>
    <li>Next, find the application from the table and go to that application release.</li>
    <li>Change state to <b>IN-REVIEW</b> -> <b>APPROVED</b> -> <b>PUBLISHED</b>.</li>
    <li>Go to the Store section of the application to view the published app.</li>
</ul>

<strong><h2>How to Add App Reviews</h2></strong>

<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Find the published application from the the app store.</li>
    <li>Go to <b>Add Review</b> section of the application to add a review.</li>
    <li>After that you can see the review details and the review rating on the bottom of the application page.</li>
</ul>

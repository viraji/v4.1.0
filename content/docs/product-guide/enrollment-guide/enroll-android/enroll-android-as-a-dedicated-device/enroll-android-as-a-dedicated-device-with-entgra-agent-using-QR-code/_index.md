---
bookCollapseSection: true
weight: 2
---

# Android Device Enrollment as a Dedicated Device with Entgra Agent using QR Code

{{< hint info >}}
<strong>Prerequisites</strong><br>
<ul style="list-style-type:disc;">
<li>Server has to be <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a>.</li>
 <li>Must have been logged on to the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">Device Management Portal</a>.</li>
<li>Installing <a href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-android/install-agent/ "> Entgra Agent</a> section should have been followed.</li>
<li>Optionally, <a href="{{< param doclink >}}key-concepts/#android ">Basic Concepts of Android Device Management</a> will be beneficial as well. </li>
<li>When enrolling a device using QR Code, the the <a  href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-android/android-configurations/"> Platform Configurations </a> for an Android device should have been first set.</li> 
 </ul>
{{< /   hint >}}



<iframe width="560" height="315" src="https://www.youtube.com/embed/4OckRsN_IYU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Set platform configurations as indicated in the prerequisites.</li>
    <li>Click <strong>Enroll Device</strong> and select the device type.</li>
    <li>On the server, select the device ownership as <strong>COSU (KIOSK)</strong>.</li>
    <li>Scan the QR code that is generated on the server.</li>
    <li>If you agree to the policy agreement is shown, click <strong>Agree</strong> to proceed.</li>
    <li>Click <strong>Allow</strong> if you agree to the request for permissions for access to the device as indicated. </li>
    <li>Click <strong>Allow</strong> if you agree to using data usage monitoring to allow the server to check the data usage of the device.</li>
    <li>Allow the agent to change <strong>Do not Disturb</strong> status which is used to ring the device.</li>
    <li>Enter and confirm a PIN code, which is required by the administrator to perform any critical tasks with user consent.  Then click <strong>Set PIN Code</strong> to complete the enrollment.</li>
    </ul>

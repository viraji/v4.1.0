---
bookCollapseSection: true
weight: 2
---

# Android Platform Configurations


Multiple tenants can use Entgra IoTS. while, maintaining tenant-based isolation, the Android 
configurations enable the tenants to customize the Android settings based on their own requirements.

{{< hint info >}}
   <strong>Prerequisites</strong>
   <br>
   <ul style="list-style-type:disc;">
      <li>Server has to be <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a>.</li>
       <li>Must have been logged on to the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">Device Management Portal</a>.</li>
      <li>Optionally, <a href="{{< param doclink >}}key-concepts/#android ">Basic Concepts of Android Device Management</a> will be beneficial as well. </li>
      </ul>
   {{< /   hint >}}
   
Follow the steps given below to configure the Android platform:   
   
1. Click the menu icon.

<img src = "../../../../image/4000.png" style="border:5px solid black ">

2. Click <strong>CONFIGURATION MANAGEMENT</strong>.

<img src = "../../../../image/4001.png" style="border:5px solid black ">

3. Click <strong>PLATFORM CONFIGURATIONS</strong>.

<img src = "../../../../image/4004.png" style="border:5px solid black ">

4. Select <strong>Android Configurations</strong>.

<img src = "../../../../image/4003.png" style="border:5px solid black ">

5. Enter the following values to the relevant fields:
   
   <ul>
       <li><strong> Notifier Type </strong>- The notifier type determines the way in which the notification will take place. The available options are as follows:
           <ul style="list-style-type:disc;">
               <li><strong> Local Polling </strong> - The device will contact the Entgra IoT server periodically. If Local Polling was selected as the notifier type, provide the Notifier Frequency (in seconds) - This is the interval after which the wake-up command will be automatically triggered by the Android Agent.
                   <img src="../../../../image/4005.png" style="border:5px solid black "> </li>
               <i> <strong> Note </strong><br> From Android 4.4 (KitKat) onwards, the  OS does 
                  not allow applications to trigger wake-up commands when the trigger period is set 
                  to less than a minute. Therefore, make sure to set the notifier frequency to 60 
                  seconds or more. </i>
               <li><strong>Firebase Cloud Messaging (FCM) </strong> - FCM will send a notification to the device when there are pending operations available for a specific device. If FCM has been selected as the notifier type, configure Entgra IoTS with Firebase Cloud Messaging (FCM).
                   <img src="../../../../image/4006.png" style="border:5px solid black "></li>
           </ul>
           <li> <strong> End User License Agreement (EULA) </strong> - Provide the license agreement that a user must adhere to when enrolling an Android device with Entgra IoTS.</li>
   </ul>
   
## Android KIOSK Provisioning Configuarions

Fill the fields with required values and click <strong>Save</strong> to apply the changes to the device.

<img src = "../../../../image/4008.png" style="border:5px solid black "> 

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Keys</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>PROVISIONING_DEVICE_ADMIN_COMPONENT_NAME</strong></td>
            <td>Package name of the Entgra Android agent. ( ex: io.entgra.iot.agent/org.wso2.iot .agent.services.AgentDeviceAdminReceiver )
            </td>
        </tr>
        <tr>
            <td><strong> PROVISIONING_DEVICE_ADMIN_PACKAGE_CHECKSUM </strong></td>
            <td>Checksum value of the android agent. ( ex: gJD2YwtOiWJHkSMkkIfLRlj-quNqG1fb6v100QmzM9w= )
            </td>
        </tr>
        <tr>
            <td>
                <strong>PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_LOCATION </strong>
            </td>
            <td>Android agent download location. ( ex: http://192.168.8.20:8080/android.apk )</td>
        </tr>
        <tr>
            <td><strong>PROVISIONING_WIFI_SSID</strong></td>
            <td>SSID of WI-FI network. ( ex: Entgra )</td>
        </tr>
        <tr>
            <td><strong>PROVISIONING_WIFI_SECURITY_TYPE</strong></td>
            <td>Wi-Fi security type of network.
                <ul>
                    <li>NONE</li>
                    <li>WPA : WiFi Protected Access</li>
                    <li>WEP : Wired Equivalent Privacy</li>
                    <li>EAP : Extensible Authentication Protocol</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td><strong>PROVISIONING_WIFI_PASSWORD</strong></td>
            <td>Password of the Wi-Fi network.</td>
        </tr>
        <tr>
            <td><strong>SERVER_ADDRESS</strong></td>
            <td>Server address ( ex: www.abc.com:8280 )</td>
        </tr>
        <tr>
            <td><strong>PROVISIONING_SKIP_ENCRYPTION</strong></td>
            <td>Set value false to enroll KIOSK device without encrypting.</td>
        </tr>
        <tr>
            <td><strong>DEFAULT_OWNERSHIP</strong></td>
            <td>
                <ul>
                    <li>BYOD</li>
                    <li>COPE</li>
                    <li>COSU</li>
                    <li>WORK_PROFILE</li>
                    <li>GOOGLE_WORK_PROFILE</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>  

## Android for Work Configurations

<i>Configure Android for Work Integration</i>

If you want to add Android Configuration for Work Integration enter the required fields and click 
 <strong>Begin Configurations</strong> to apply the changes.
 
 <img src = "../../../../image/4009.png" style="border:5px solid black ">

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Keys</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td>
                <strong>Server details provided by EMM vendor</strong></td>
            <td></td>
        </tr>
        <tr>
            <td><strong>Token</strong></td>
            <td></td>
        </tr>
        <tr>
            <td>
                <strong>ESA</strong>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                <strong>Enterprise ID</strong>
            </td>
            <td></td>
        </tr>
    </tbody>
</table> 

## Unenroll Enterprise from EMM

If you want to Unregister your enterprise from EMM,  click <strong>Unenroll</strong>. This action cannot be undone.

<img src = "../../../../image/4007.png" style="border:5px solid black ">   
   


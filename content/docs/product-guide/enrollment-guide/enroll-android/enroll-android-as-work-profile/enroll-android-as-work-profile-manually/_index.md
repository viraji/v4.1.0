---
bookCollapseSection: true
weight: 1
---

# Android Device Manual Enrollment as Work Profile


{{< hint info >}}
<strong>Prerequisites</strong><br>
<ul style="list-style-type:disc;">
   
   <li>Server has to be <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a>.</li>
       <li>Must have been logged on to the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">Device Management Portal</a>.</li>
       <li>Installing <a href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-android/install-agent/ "> Entgra Agent</a> section should have been followed.</li>
       <li>Optionally, <a href="{{< param doclink >}}key-concepts/#android ">Basic Concepts of Android Device Management</a> will be beneficial as well. </li>
       </ul>
{{< /   hint >}}



<iframe width="560" height="315" src="https://www.youtube.com/embed/6hX8ZidTdYw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<h2>Steps</h2>

<ul style="list-style-type:decimal;">
   <li>Open the Entgra Agent application.</li>
          <li>Click <strong>Continue</strong> after reading the Disclaimer that appears.</li>
          <li>On the screen that appears next, click <strong>Manual Enrollment</strong>.</li>
       <li>Click <strong>BYOD Enrollment</strong> next in the screen that follows up.</li>
    <li>Next, click <strong>Setup Work-Profile</strong>.</li>
    <li>Click <strong>Continue</strong> to proceed.</li>
     <li>Type in the server address which is the IP of the server and port as <strong>8280</strong>. Click <strong>Start Registration</strong>.</li>
        <li>Type in the Username: <strong>admin</strong>, Password: <strong>admin</strong> then click <strong>Sign In</strong>.</li>
        <li>If you agree to the policy agreement that is shown next, click <strong>Agree</strong> to proceed.</li>
        <li>Click <strong>Activate</strong> in the device screen.</li>
            <li>Click <strong>Allow</strong> if you agree to the request for permissions for access to the device as indicated. </li>
            <li>Click <strong>Allow</strong> if you agree to using data usage monitoring to allow the server to check the data usage of the device.</li>
            <li>Allow the agent to change <strong>Do not Disturb</strong> status which is used to ring the device.</li>
            <li>Enter and confirm a PIN code, which is required by the administrator to perform any critical tasks with user consent.  Then click <strong>Set PIN Code</strong> to complete the enrollment.</li>
</ul>

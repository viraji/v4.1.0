---
bookCollapseSection: true
weight: 2
---

# Android Device Enrollment as Fully Managed using QR Code

{{< hint info >}}
<strong>Prerequisites</strong>
<ul style="list-style-type:disc;">
 
 <li>Server has to be <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a>.</li>
 <li>Must have been logged on to the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">Device Management Portal</a>.</li>
<li>Installing <a href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-android/install-agent/ "> Entgra Agent</a> section should have been followed.</li>
<li>Optionally, <a href="{{< param doclink >}}key-concepts/#android ">Basic Concepts of Android Device Management</a> will be beneficial as well. </li>
<li>When enrolling a device using QR Code, the the <a  href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-android/android-configurations/"> Platform Configurations </a> for an Android device should have been first set.</li> 
 </ul>
{{< /hint >}}

<iframe width="560" height="315" src="https://www.youtube.com/embed/hux4WJVN4ho" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Open the Entgra Agent application.</li>
       <li>Click <strong>Continue</strong> after reading the Disclaimer that appears.</li>
       <li>In the screen that follows, click <strong>Enroll with QR Code</strong>.</li>
       <li>In the server, select device ownership as <strong>COPE</strong>.</li>
          <li>Scan QR code that is generated on the server.</li>
         <li>The enrollment will be on hold until user enables the device owner:</li>
    <li>Enter the following command in the terminal or on the console to enable device owner.
        <br><i>adb shell 
    dpm 
    set-device-owner io.entgra.iot.agent/org.wso2</i> .iot.agent.services.AgentDeviceAdminReceiver
    </li>
    <li>Click <strong>Agree</strong> in the screen that appears, if you agree to and accept the licence.</li>
</ul>
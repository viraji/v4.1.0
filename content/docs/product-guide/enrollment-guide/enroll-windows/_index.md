---
bookCollapseSection: true
weight: 5
---

# Windows server configuration

The Entgra IoTS client sends requests to the Entgra IoTS server through a Proxy Server. The Windows Entgra IoTS protocol constructs a URI that uses the host name by appending the domain of the email address to the subdomain, enterpriseenrollment for the each device request. Therefore, you can either purchase a domain name or create a DNS entry in the  http://enterpriseenrollment.<EMAIL_DOMAIN> format.

For example, discover the Entgra IoT server by sending a request through: http://enterpriseenrollment.<EMAIL_DOMAIN>/ENROLLMENTSERVER/Discovery.

As the Entgra IoT server can not create Windows service endpoints it is advisable to use a proxy server between the device and the Entgra IoT server.




The following subsections are given as examples to guide you on how to configure the proxy server for Windows. You can use any proxy server for this task but we recommend that you use NGINX as it is simple to try out.

{{< expand "Nginx Configuration" "..." >}}
A preferred server can be used as a proxy server between the device and the Entgra IoT server. The steps documented below is only an example of configuring the proxy server using NGINX, which is a known reverse proxy server.

Follow the steps given below to configure the proxy Server:

1. Install NGINX in your production environment. Refer the following to install NGINX on a MAC or Ubuntu environment.


  * [nginx installation guide](https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-open-source/)


2. Get an SSL certificate. Make sure that the common name of the certificate you are getting matches the constructed URI.

 {{< hint info >}}
The Entgra IoTS client sends requests to the Entgra IoTS server through a Proxy Server. The Windows Entgra IoTS protocol
constructs a URI that uses the hostname by appending the domain of the email address to the subdomain, enterpriseenrollment
for each device request. Therefore, you can either purchase a domain name or create a DNS entry in the http://enterpriseenrollment.EMAIL_DOMAIN format.
{{< /hint >}}

### To create the SSL certificate
* Create a file called openssl.cnf with the below content
```
[req]
distinguished_name = req_distinguished_name
req_extensions = v3_req
[req_distinguished_name]
countryName = SL
countryName_default = SL
stateOrProvinceName = Western
stateOrProvinceName_default = Western
localityName = Colombo
localityName_default = Colombo
organizationalUnitName = ABC
organizationalUnitName_default = ABC
commonName = dev.abc.com
commonName_max = 64
[ v3_req ]
# Extensions to add to a certificate request
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = dev.abc.com
DNS.2 = enterpriseenrollment.dev.abc.com
```

{{< hint info >}}
Add any domain name needed to subject alternatives - DNS
{{< /hint >}}


* Create the Private key-
```
openssl genrsa -out server.key 2048
```
* Create a Certificate Signing Request (CSR). Press enter to every input.
```
openssl req -new -out server.csr -key server.key -config openssl.cnf
```
* Sign the SSL Certificate.
```
 openssl x509 -req -days 3650 -in server.csr -signkey server.key -out server.crt -extensions v3_req -extfile openssl.cnf
```


3. Navigate to the /usr/local/etc/nginx directory, create a folder named ssl, and add the CA certificate and the private key to this folder.

4. Configure the /usr/local/etc/nginx/nginx.conf file with the details of the SSL certificate and the Windows endpoints as explained below.

    a. Compare the sample configuration file given below with your nginx.conf file and add the missing properties.

{{< hint warning >}}
What's given below is only an example. Compare your configuration file with what's given below and add the missing configurations or uncomment the commented configurations in the nginx.conf file.
{{< /hint >}}

```
#user  nobody;
worker_processes  1;
#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;
#pid        logs/nginx.pid;
events {
worker_connections  1024;
}
http {
include       mime.types;
default_type  application/octet-stream;
#log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
#                  '$status $body_bytes_sent "$http_referer" '
#                  '"$http_user_agent" "$http_x_forwarded_for"';
#access_log  logs/access.log  main;
sendfile        on;
#tcp_nopush     on;
#keepalive_timeout  0;
keepalive_timeout  65;
#gzip  on;
server {
listen       8080;
server_name  localhost;
#charset koi8-r;
#access_log  logs/host.access.log  main;
location / {
root   html;
index  index.html index.htm;
}
#error_page  404              /404.html;
# redirect server error pages to the static page /50x.html
#
error_page   500 502 503 504  /50x.html;
location = /50x.html {
root   html;
}
# proxy the PHP scripts to Apache listening on 127.0.0.1:80
#
#location ~ \.php$ {
#    proxy_pass   http://127.0.0.1;
#}
# pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
#
#location ~ \.php$ {
#    root           html;
#    fastcgi_pass   127.0.0.1:9000;
#    fastcgi_index  index.php;
#    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
#    include        fastcgi_params;
#}
# deny access to .htaccess files, if Apache's document root
# concurs with nginx's one
#
#location ~ /\.ht {
#    deny  all;
#}
}
# another virtual host using mix of IP-, name-, and port-based configuration
#
#server {
#    listen       8000;
# listen       enterpriseenrollment.dilan.me;
#    server_name  somename  alias  another.alias;
#    location / {
#        root   html;
#        index  index.html index.htm;
#    }
#}
# HTTPS server
#
#server {
#    listen       443 ssl;
#    server_name  localhost;
#    ssl_certificate      cert.pem;
#    ssl_certificate_key  cert.key;
#    ssl_session_cache    shared:SSL:1m;
#    ssl_session_timeout  5m;
#    ssl_ciphers  HIGH:!aNULL:!MD5;
#    ssl_prefer_server_ciphers  on;
#    location / {
#        root   html;
#        index  index.html index.htm;
#    }
#}
server {
listen 443 ssl;
server_name enterpriseenrollment.dilan.me;
ssl on;
ssl_certificate /usr/local/etc/nginx/ssl/certificate.crt;
ssl_certificate_key /usr/local/etc/nginx/ssl/private.key;
location /EnrollmentServer/Discovery.svc {
if ($request_method = GET) {
return 200;
}
proxy_set_header X-Forwarded-Host $host:$server_port;
proxy_set_header X-Forwarded-Server $host;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_pass http://192.168.8.100:8280/api/device-mgt/windows/v1.0/discovery/post;
proxy_http_version 1.1;
}
location /ENROLLMENTSERVER/PolicyEnrollmentWebservice.svc {
proxy_set_header X-Forwarded-Host $host:$server_port;
proxy_set_header X-Forwarded-Server $host;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_pass http://192.168.8.100:8280/api/device-mgt/windows/v1.0/certificatepolicy/xcep/1.0.0;
proxy_http_version 1.1;
}
location /windows-web-agent {
proxy_set_header X-Forwarded-Host $host:$server_port;
proxy_set_header X-Forwarded-Server $host;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_pass http://192.168.8.100:9763/windows-web-agent;

proxy_http_version 1.1;
}
location /ENROLLMENTSERVER/DeviceEnrollmentWebservice.svc {
proxy_set_header X-Forwarded-Host $host:$server_port;
proxy_set_header X-Forwarded-Server $host;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_pass http://192.168.8.100:8280/api/device-mgt/windows/v1.0/deviceenrolment/wstep/;
proxy_http_version 1.1;
}
location /ENROLLMENTSERVER/Win10DeviceEnrollmentWebservice.svc {
proxy_set_header X-Forwarded-Host $host:$server_port;
proxy_set_header X-Forwarded-Server $host;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_pass http://192.168.8.100:8280/api/device-mgt/windows/v1.0/deviceenrolment/enrollment;

proxy_http_version 1.1;
}
location /Syncml/initialquery {
proxy_set_header X-Forwarded-Host $host:$server_port;
proxy_set_header X-Forwarded-Server $host;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_pass http://192.168.8.100:8280/api/device-mgt/windows/v1.0/syncml/devicemanagement/1.0.0/request/;
proxy_http_version 1.1;
}
location /devicemgt {
proxy_set_header X-Forwarded-Host $host:$server_port;
proxy_set_header X-Forwarded-Server $host;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_pass http://192.168.8.100:8280/api/device-mgt/windows/v1.0/management/devicemgt/1.0.0/pending-operations/;
proxy_http_version 1.1;
}
}
include servers/*;
}
```

    b. Configure the SSL certificate details.
```
server {
        listen 443;
        server_name enterpriseenrollment.wso2.com;
        ssl on;
        ssl_certificate /usr/local/etc/nginx/ssl/star_wso2_com.crt;
        ssl_certificate_key /usr/local/etc/nginx/ssl/enterpriseenrollment_wso2_com.key;
```



You need to configure the following properties:

|Property           |Description                                               |Example                                                   |
|-------------------|----------------------------------------------------------|----------------------------------------------------------|
|server_name        |Define the common name of the certificate.                |enterpriseenrollment.wso2.com                             |
|ssl_certificate    |Define where you saved the SSL certificate.               |/usr/local/etc/nginx/ssl/wso2_com_SSL.crt                 |
|ssl_certificate_key|Define where you saved the private key of the certificate.|/usr/local/etc/nginx/ssl/enterpriseenrollment_wso2_com.key|

c. Configure the Windows endpoints.

* The GATEWAY_PORT used by default in Entgra IoTS for device management is 8280.
* For more information on how the Windows endpoints are used, see [how the Windows device enrollment process message flow works](Windows-device-enrollment-message-flow).

Example:
```
location /ENROLLMENTSERVER/PolicyEnrollmentWebservice.svc {
    proxy_set_header X-Forwarded-Host $host:$server_port;
    proxy_set_header X-Forwarded-Server $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    proxy_pass http://10.10.10.10:8280/api/device-mgt/windows/v1.0/certificatepolicy/xcep;

    proxy_http_version 1.1;
}
```

|Property|Description|
|--- |--- |
|location|This property specifies the "/" prefix that needs to be compared with the URI sent from the request. For more information, see the NGINX documentation.|
|proxy_set_header|Required to configure Windows for reverse proxy.|
|proxy_pass|Define the respective Windows endpoint.|
{{< /expand >}}


{{< expand "Apache2 HTTP configuration" "..." >}}


Any preferred server can be used as a proxy server between the device and the Entgra IoT server. The steps documented below is only an example of configuring the proxy server by using the Apache2 HTTP Server. The Apache Server can be configured using the forward or reverse proxy (also known as gateway) mode. The reverse proxy mode is used to configure Apache2\. You can download the Apache2 HTTP Server from [here](https://httpd.apache.org/download.cgi).

Follow the steps given below to configure the proxy Server:

## Step 1: Configure reverse proxy

A reverse proxy (or gateway) appears to the client like an ordinary web server with no special configuration required for the client. Ordinary requests for content is made by the client through the `name-space`. The reverse proxy redirects the requests, and returns the required output.



The following modules are required to configure the reverse proxy:

*   `mod_proxy.so`
    This module deals with proxying in Apache.

*   `mod_proxy_http.so` This module handles connections with both the HTTP and HTTPS protocols.

1.  Navigate to the `etc/apache2` directory and use the following command to enable the above modules:


    cd /etc/apache2
    a2enmod proxy_http

2.  Configure the `proxy.conf` file that is in the `/etc/apache2/mods-available` directory by including the configurations given below to the end of the file.

```
    ServerName localhost
    ProxyRequests off
    ProxyPreserveHost off
      <Proxy *>
      Order deny,allow
      #Deny from all
      Allow from all
      </Proxy>
    ProxyPass /ENROLLMENTSERVER/PolicyEnrollmentWebservice.svc http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/certificatepolicy/xcep
    ProxyPassReverse /ENROLLMENTSERVER/PolicyEnrollmentWebservice.svc http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/certificatepolicy/xcep

    ProxyPass /ENROLLMENTSERVER/DeviceEnrollmentWebservice.svc http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/deviceenrolment/wstep
    ProxyPassReverse /ENROLLMENTSERVER/DeviceEnrollmentWebservice.svc http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/deviceenrolment/wstep

    ProxyPass /Syncml/initialquery http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/syncml/devicemanagement/request
    ProxyPassReverse /Syncml/initialquery http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/syncml/devicemanagement/request

    ProxyPass /ENROLLMENTSERVER/Win10DeviceEnrollmentWebservice.svc http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/deviceenrolment/enrollment
    ProxyPassReverse /ENROLLMENTSERVER/Win10DeviceEnrollmentWebservice.svc http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/deviceenrolment/enrollment

    ProxyPass /devicemgt  http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/management/devicemgt/pending-operations
    ProxyPassReverse /devicemgt  http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/management/devicemgt/pending-operations

    ProxyPass /windows-web-agent http://server-ip>:<server-port>/windows-web-agent
    ProxyPassReverse /windows-web-agent http://server-ip>:<server-port>/windows-web-agent


    The default `<server-ip>:<server-port>` is `localhost:9443.`

```


## Step 2: Configure the Rewrite engine


The first `GET` and `POST` HTTP requests are received by the same MDM endpoint and the rewrite conditions filter the device requests. By default the Apache Rewrite engine is disabled.


Follow the steps given below to enable the Rewrite engine when running on Ubuntu:

1.  Invoke the rewrite rules:

    1.  Create a `.htaccess` file in the `/var/www/` directory with the specific rewrite rules.
    2.  Enable the `mod_rewrite` module.

        `sudo a2enmod rewrite`

2.  Configure the `000-default` file, which is in the `/etc/apache2/sites-enabled` directory.


    This step is required to replicate the configuration changes required in the Apache versions on a few files.In the older Apache versions, all virtual host directory directives were managed in the `apache2.conf` file, which is in the `/etc/apache2` directory. In the Apache 2.4.7 version this has changed and the alterations are handled within the `/etc/apache2/sites-enabled` directory.

    1.  Configure the value assigned to `AllowOveride` from `None` to `All` under `<Directory/>`.

        `AllowOverride All`

    2.  Configure the content under `<Directory /var/www/>`.


        If the `000-default` file does not contain the `Directory` tag you need to add it to the file as shown below:


        <Directory /var/www/>
         Options Indexes FollowSymLinks MultiViews
         AllowOverride all
         Order allow,deny
         allow from all
        </Directory>


3.  Restart the Apache server.

    `server.sudo service apache2 restart`



**Rewrite Engine Configuration file format**


RewriteEngine on
RewriteCond %{REQUEST_METHOD} ^(GET)$
RewriteRule /EnrollmentServer/Discovery.svc   http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/discovery/get [P,L]

RewriteCond %{REQUEST_METHOD} ^(POST)$
RewriteRule /EnrollmentServer/Discovery.svc http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/discovery/post [P,L]


The default `<server-ip>:<server-port>` is `localhost:9443.`





## Step 3: SSL configurations for Apache2


An SSL certificate is used to encrypt the information of a site and create a secure connection.



Follow the steps given below to configure SSL for Apache2:


SSL support is available as a standard on the Ubuntu 14.04 Apache package.



1.  Enable the SSL Module.

    `sudo a2enmod ssl`

2.  Create a subdirectory named `ssl` within the Apache server configuration hierarchy to place the certificate files.

    `sudo mkdir /etc/apache2/ssl`


    The Entgra IoTS certificate must be generated from a trusted authority.

    Once you have the Entgra IoT certificate and key available, configure the Apache server to use these files in a virtual host file. For more information, see[ how to set up Apache virtual hosts](https://www.digitalocean.com/community/articles/how-to-set-up-apache-virtual-hosts-on-ubuntu-14-04-lts).





3.  Configure the `default-ssl.conf` file, which is in the `/etc/apache2/sites-enabled` directory contains the default SSL configurations.


    SSLEngine on
    SSLCertificateFile    /etc/apache2/ssl/<COMAPNY_CERTIFICATE>
    SSLCertificateKeyFile /etc/apache2/ssl/<COMPANY_PUBLIC_KEY>
    SSLCACertificateFile /etc/apache2/ssl/<COMPANY_ROOT_CERTIFICATE>


    Example:


    SSLEngine on
    SSLCertificateFile    /etc/apache2/ssl/star_wso2_com.crt
    SSLCertificateKeyFile /etc/apache2/ssl/star_wso2_com.key
    SSLCACertificateFile /etc/apache2/ssl/DigiCertCA.crt


4.  Enable the SSL-enabled virtual host that you configured in the above step.

    `sudo a2ensite default-ssl.conf`

5.  Restart the Apache server to load the new virtual host file.

    `sudo service apache2 restart`

{{< /expand >}}

{{< expand "Enrolling the Windows device" "..." >}}

# Enrolling a windows device

1. add an entry to the windows host files, for example:
```
192.168.8.100   dev.abc.com
192.168.8.100   enterpriseenrollment.dev.abc.com
```
1. Go to windows Settings > Accounts > Access to work or school > Enroll only in device management

1. A new window will appear to setup a work or school account. enter the email as:
```
admin@dev.abc.com
```
{{< hint info >}}
dev.abc.com is the dns provided when setting up the host file.
{{< /hint >}}

1. once the email is submitted, the wizard will search for the organization and open a new window to log in to the IoT server admin. Once the login process is completed, it will enroll the device sync the policies

{{< /expand >}}

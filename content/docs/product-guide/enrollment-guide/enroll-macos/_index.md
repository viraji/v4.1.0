---
bookCollapseSection: true
weight: 4
title: "Enroll macOS"
---

# Enroll macOS Device Manually

{{< hint info >}}
<strong>Prerequisites</strong>
<ul style="list-style-type:disc;">
    <li>Server has to be <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started.</a></li>
    <li>Must have been logged on to the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">Device Management Portal</a>.</li>
    <li>Click <strong>Add device</strong> <strong>(https://{IP}:{port}/devicemgt/device/enroll)</strong></li>
    <li>Click <strong>iOS</strong> from <strong>DEVICE TYPES</strong>.</li>
    <li>Type <strong>https://{IP}:{port}/ios-web-agent/enrollment</strong> in safari browser.</li>
</ul>
{{< /hint >}}


<iframe width="560" height="315" src="https://www.youtube.com/embed/5q478-OIWGE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Copy the URL of QR code to the browser of the mac OS device.</li>
    <li>Click <strong>Enroll without agent</strong>.</li>
    <li>Click <strong>Install IoT Server Certificate</strong>.</li>
    <li>Open <strong>Key Chain Acess</strong> Application</li>
    <li>Open the downloaded IoT Server Certificate.</li>
    <li>Set trust to <strong>Always trust</strong> for the downloaded IoT Server Certificate.</li>
    <li>Click <strong>Next</strong>.</li>
    <li>Type the Username: admin, Password: admin then click <strong>Sign in</strong>.</li>
    <li>Accept the Licence Agreement to continue.</li>
    <li>You will be prompted to confirm the installation of the profile to the device. Then click <strong>Install</strong> to install the profile to the device.</li>
    <li>You will be prompted to confirm the installation of the Mobile Device Management profile. Then click <strong>Install</strong> to install the Mobile Device Management profile to the device.
    </li>
</ul>

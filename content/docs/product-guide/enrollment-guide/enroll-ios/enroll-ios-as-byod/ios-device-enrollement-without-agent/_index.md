---
bookCollapseSection: true
weight: 3
---

# iOS Device Enrollment (BYOD) without Agent

{{< hint info >}}
<strong>Prerequisites</strong>
<ul style="list-style-type:disc;">
     <li>Server has to be <a href="{{< param doclink >}}product-guide/download-and-start-the-server/">downloaded and started</a>.</li>
       <li>Must have been logged on to the server's <a href="{{< param doclink >}}product-guide/login-guide/">Device Management Portal</a>.</li>
       <li>Click <strong>Add Device</strong> (https://{IP}:{port}/devicemgt/device/enroll)</li>
        <li>Click <strong>iOS</strong> under <strong>DEVICE TYPES</strong></li>
    <li>Scan the QR code that appear with a QR code scanning app or type 
    <strong>https://{IP}:{port}/ios-web-agent/enrollment in safari browser</strong>.</li>
</ul>
{{< /hint >}}

## If the Device is above iOS 12.2 
(If you have installed OS updates after March 2019)


<iframe width="560" height="315" src="https://www.youtube.com/embed/CM_LUgdon4w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
</iframe>


## If the Device is bellow iOS 12.2

<iframe width="560" height="315" src="https://www.youtube.com/embed/udLp7ErxwuY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
</iframe>
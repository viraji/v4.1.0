---
bookCollapseSection: true
weight: 1
---

# General Platform Configurations

The general platform configurations for Entgra IoT Server involves setting up configurations for monitoring how often the devices are enrolled with the server and for deploying the geo-analytics artifacts required for Location-Based Services [LBS] in a multi-tenant environment.

Follow the instructions below to configure the general platform settings:

1.  [Sign in to the Entgra IoTS Device Management Console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/) and click the menu icon.

    <img src = "../../../image/352821220.png" style="border:5px solid black ">
    
2.  Click **Platform Configurations**.

    <img src = "../../../image/352821214.png" style="border:5px solid black ">
    
3.  Define the **Monitoring Frequency** in seconds, to monitor the enforced policies on the devices, and click  **SAVE**.  
    
    <img src = "../../../image/352821230.png" style="border:5px solid black ">
    
4.  The geo-analytics artifacts are deployed by default in the Entgra IoT Server super tenant. However, if you are setting up geofencing or location based services in a multi-tenant environment, you have to deploy the geo-analytics artifacts in each tenant. 

    1.Click the menu icon.
    
    <img src = "../../../image/4000.png" style="border:5px solid black ">
    
    2.Click **CONFIGURATION MANAGEMENT**.
    
    <img src = "../../../image/4001.png" style="border:5px solid black ">
    
    3.Click **PLATFORM CONFIGURATIONS**.
    
    <img src = "../../../image/4004.png" style="border:5px solid black ">

    4.  Click **Deploy Geo Analytics Artifacts**. You can also use this button to 
        re-deploy the geo-analytics artifacts in super tenant mode if required.   
        
        <img src = "../../../image/352821235.png" style="border:5px solid black ">

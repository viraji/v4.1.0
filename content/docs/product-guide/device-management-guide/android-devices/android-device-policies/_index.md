---
bookCollapseSection: true
weight: 1
---

# Android Device Policies

## Add a Policy

{{< hint info >}}
   <strong>Prerequisites</strong>
   <br>
   <ul style="list-style-type:disc;">
       <li>The server has to be <a href="{{< param doclink >}}product-guide/download-and-start-the-server/">downloaded and started.</a></li>
       <li>Must have been logged on to the server's <a href="{{< param doclink >}}product-guide/login-guide/">Device Management Portal.</a></li>
   </ul>
   {{< /   hint >}}

1. Click **Add Policies** (https://{IP}:{port}/devicemgt/policy/add) 

<img src="../../../../image/60019.png" style="border:5px solid black" >

2. Click **Android** from in the DEVICE TYPES.

<img src="../../../../image/60020.png" style="border:5px solid black" >

3. Create your policy. In this tutorial, let's create a passcode policy.
   After defining the settings, click **CONTINUE**.
   
      A profile in the context of Entgra IoT Server refers to a collection of policies.
      For example, in this use case you are only creating one policy that is the passcode policy.
      If you want to, you can add an restrictions policy too. 
      All these policies will be bundled as a profile and then pushed to the devices.
      
4. Select the policy type.

There are two types of policies.
<ul style="list-style-type:upper-roman;">
    <li><strong>General Policy:</strong> General policy is applied to the device by default.</li>
    <li><strong>Corrective Policy:</strong> Corrective policy is applied to the device when the 
    general policy is violated. When the general policy is not violated the correctiv policy is 
    disabled.
    </li>   
</ul>

If you wish to apply a corrective policy with a general policy;

First apply a corrective policy by selecting the policy type as the corrective policy.

<img src="../../../../image/60033.png" style="border:5px solid black" >

Then apply a general policy by selecting the policy type as the general policy.

<img src="../../../../image/60034.png" style="border:5px solid black" >

Select the corrective policy to be applied when this general policy is violated.

<img src="../../../../image/60034.png" style="border:5px solid black" >

5. Click **CONTINUE**.
   
6. Define the user groups that the passcode policy needs to be assigned to:
      
      Select **Set user role(s)** or **Set user(s)** option and then select the users/roles from the item 
      list.
      Let us select **Set user roles** and then select **ANY** here. 
      
<img src="../../../../image/60020.png" style="border:5px solid black" >

      Select the Select Groups option and then select the groups from the item 
      list.
<img src="../../../../image/60018.png" style="border:5px solid black" >

7. Click **CONTINUE**.

8. Define the policy name and the description of the policy.

<img src="../../../../image/60021.png" style="border:5px solid black" >

9. Click **SAVE AND PUBLISH** to save and publish the configured profile as an active policy to the 
database.
           
    If you save the configured profile, it will be in the Inactive state and will not be applied 
    to any devices. 
    If you save and publish the configured profile of policies, it will be in Active state.   

10. To publish the policy to the existing devices, click **APPLY CHANGES TO DEVICES** from the policy 
  management page.
  
  
## View a Policy

1.Go to the Device Management portal and click **View Policies**. (https://{IP}:{port}/devicemgt/devicemgt/policies

<img src ="../../../../image/60022.png" style="border:5px solid black ">

## Publish a Policy

1. Click **View** under POLICIES to get the list of the available policies.

<img src = "../../../../image/60022.png" style="border:5px solid black ">

2. Click **Select** to select the policy or policies that are not in the published state but need to 
be published now.

<img src = "../../../../image/60025.png" style="border:5px solid black ">

3. Click **Publish**.

<img src = "../../../../image/60024.png" style="border:5px solid black ">

## Unpublish a Policy

1. Go to the Device Management Portal and click **View** policies. 
(https://{IP}:{port}/devicemgt/devicemgt/policies

<img src = "../../../../image/60022.png" style="border:5px solid black ">

2. Click **Select** to select the policy or policies that are in the Published state and need to be Unpublished now.

<img src = "../../../../image/60023.png" style="border:5px solid black ">

3. Click **Unpublish**.

<img src = "../../../../image/60026.png" style="border:5px solid black ">

4. Click **YES** to confirm that you want to unpublish the selected policy.

<img src = "../../../../image/60027.png" style="border:5px solid black ">

5. Now your policy is unpublished and is in Inactive/Updated state. Therefore, the policy will
 now not be applied on the devices that are enrolled with the Entgra IoT Server.
 
<img src = "../../../../image/60028.png" style="border:5px solid black ">

## Verify the Policy Enforced on a Device

1. Click **View** under DEVICES.

<img src = "../../../../image/60029.png" style="border:5px solid black ">

2. Click on your device to view the device details. Click **Policy Compliance**.

3. You will see the policy that is currently applied to your device.

## Manage the Policy Priority Order

You can change the priority order of the policies and make sure the policy that you want is applied 
on devices that register with Entgra IoT Server. 

1. Click **View** under POLICIES to get the list of the available policies.

<img src = "../../../../image/60022.png" style="border:5px solid black ">

2. Click **POLICY PRIORITY**.

<img src = "../../../../image/60030.png" style="border:5px solid black ">

3. Manage the policy priority:
    Drag and drop the policies to prioritize the policies accordingly.
    Manage the policy priority order by defining the order using the edit box.   
    <img src = "../../../../image/60031.png" style="border:5px solid black ">
    
4. Click **SAVE NEW PRIORITY ORDER** to save the changes. 

5. Click **APPLY CHANGES** to push the changes to the existing devices.

## Updating a Policy

1. Click **View** under POLICIES to get the list of the available policies.

<img src = "../../../../image/60022.png" style="border:5px solid black ">

2. On the policy that you wish to edit, click **Edit**.

<img src = "../../../../image/60032.png" style="border:5px solid black ">

3. Edit the Policy:

    a. Edit the current profile and click CONTINUE. <br>
    b. Edit assignment groups and click CONTINUE.   <br>
    c. Optionally, edit the policy name and description.
    
  Click **SAVE** to save the configured profile or click **SAVE AND PUBLISH** to save and publish the 
  configured profile as an Active policy to the database.

## Description of Available Policies for Android Devices
  
<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th>Policy</th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong><a href ="{{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/passcode-policy/">Passcode 
              Policy</a></strong></td>
            <td>Enforce a configured passcode policy on Android devices. Once this profile is applied, the device owners won't be able to modify the password settings on their devices.
            </td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/restrictions/">Restrictions</a></strong></td>
            <td>Restrict predefined settings on Android devices. Once this profile is applied, the device owners won't be able to modify the configured settings on their devices.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/encryption-settings/">Encryption Settings</a></strong></td>
            <td>Encrypt data on an Android device when the device is locked and make it readable when the device is unlocked. Once this profile is applied, the device owners won't be able to modify the configured settings on their devices.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/wi-fi-settings/">Wi-Fi Settings</a></strong></td>
            <td>Configure the Wi-Fi settings on Android devices. Once this profile is applied, the device owners won't be able to modify the configured settings on their devices.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/global-proxy-settings/">Global Proxy Settings</a></strong></td>
            <td>This configurations can be used to set a network-independent global HTTP proxy on an Android device. Once this configuration profile is installed on a device, all the network traffic will be routed through the proxy server.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/virtual-private-network/">Virtual 
            Private Network</a></strong></td>
            <td>These configurations can be used to define settings for connecting to your POP or IMAP email accounts. Once this configuration profile is installed on an iOS device, corresponding users will not be able to modify these settings on their devices .
            </td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/certificate-install/">Certificate Install Settings</a></strong></td>
            <td>Restrict predefined settings on Android devices. Once this profile is applied, the device owners won't be able to modify the configured settings on their devices.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/c-o-s-u-profile-configuration/">Work-Profile Configurations</a></strong></td>
            <td>Configure these settings to manage the applications in the work profile.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/c-o-s-u-profile-configuration/">COSU Profile Configuration</a></strong></td>
            <td>This policy can be used to configure the profile of COSU Devices.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/application-restriction-settings/">Application Restriction Settings</a></strong></td>
            <td>Blacklist or whitelist mobile application for Android devices.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/runtime-permission-policy/">Runtime Permission Policy (COSU / Work Profile)</a></strong></td>
            <td>This configuration can be used to set a runtime permission policy to an Android Device.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/system-update-policy/">System Update Policy (COSU)</a></strong></td>
            <td>Configure the settings to install system updates on single-purpose or COSU devices.</td>
        </tr>
        <tr>
            <td><strong><a href = "{{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/enrollment-application-install/">Enrollment Application Install</a></strong></td>
            <td>Enforce applications to be installed during Android device enrollment.</td>
        </tr>
    </tbody>
</table>
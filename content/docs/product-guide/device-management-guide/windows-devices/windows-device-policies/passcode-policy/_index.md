---
bookCollapseSection: true
weight: 1
---
# Passcode policy

After changing passcode, users will be prompted to change the passcode on the next reboot. If not, passcode can be changed by going to,
Windows Menu -> Settings -> Accounts

<img src="../../../../../image/windows_policy_1.png" style="border:5px solid black">

Click Password -> Change

<img src="../../../../../image/windows_policy_2.png" style="border:5px solid black">

When a passcode that does not match the criteria, following error will be displayed when changing passcode.

<img src="../../../../../image/windows_policy_3.png" style="border:5px solid black">

Disable device reset -
Disable one drive sync - To validate on Desktop, do the following:
            
            1. Enable policy.
            
            2. Restart machine.
            
            3. Verify that OneDrive.exe is not running in Task Manager.

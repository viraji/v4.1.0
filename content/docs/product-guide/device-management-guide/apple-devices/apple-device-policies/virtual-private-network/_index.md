---
bookCollapseSection: true
weight: 15
---

# VPN(Virtual Private Network) Settings

{{< hint info >}}
<b> <a href ="{{< param doclink >}}product-guide/device-management-guide/apple-devices/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an iOS device.
{{< /hint >}}

This configurations can be used to configure VPN settings on an iOS device. Once this configuration 
profile is installed on a device, corresponding users will not be able to modify these settings on their devices.

<i>Please note that * sign represents required fields of data.</i>


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Connection Name</strong></td>
            <td>Description of the VPN connection displayed on the device.
            </td>
        </tr>
        <tr>
            <td><strong>Override Primary</strong></td>
            <td>Specifies whether to send all traffic through the VPN interface. If true, all network traffic is sent over VPN.</td>
        </tr>
        <tr>
            <td><strong>On-demand Enabled</strong></td>
            <td>Check if the VPN connection should be brought up on demand, else leave un-checked.
            </td>
        </tr>
        <tr>
            <td><strong>VPN Type</strong></td>
            <td>Determines the settings available in the payload for this type of VPN connection. It can have one of the following values:
                <ul style="list-style-type:disc;">
                    <li>L2TP</li>
                    <li>PPTP</li>
                    <li>IPSec (Cisco)</li>
                    <li> IKEv2 (see IKEv2 Dictionary Keys)</li>
                    <li>AlwaysOn (see AlwaysOn Dictionary Keys)</li>
                    <li> VPN (solution uses a VPN plugin or NetworkExtension,</li>
                    so the VPNSubType key is required (see below)).
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="2"><strong><center>Vendor Configurations</strong>
                <center>
            </td>
        </tr>
        <tr>
            <td><strong>Key</strong></td>
            <td></td>
        </tr>
        <tr>
            <td><strong>Value</strong></td>
            <td></td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}product-guide/device-management-guide/apple-devices/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}

---
bookCollapseSection: true
weight: 2
---
# Add operation to an apple device

{{< hint info >}}
   <strong>Pre-requisites</strong>
   <br>
   <ul style="list-style-type:disc;">
       <li>Server is <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a></li>
       <li>Logged into the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">device mgt portal</a></li>
       <li><a href="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/manage-an-enroll-device/#view-an-enrolled-device">View </a>the device that you have 
       enrolled.</li> 
   </ul>
   {{< /   hint >}}

1.Click on the operation that you need to apply to the device. In 
this tutorial, let's apply 
Voice roaming operation.

2.Then a pop up message will be displayed on the screen. If you need to enable voice roaming on 
the  mac OS device, you have to select the checkbox. If you deselect the check box that will 
disable voice roaming in the device.

<img src="../../../../image/60036.png" style="border:5px solid black">

3.And click on The button to confirm the operation.

<img src="../../../../image/60037.png" style="border:5px solid black">

The following table lists out the operations that can be applied to the macOS device

<table style="width: 100%;">
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Operation Type</th>
      <th>Description</th>
    </tr>
    <tr>
    </tr>
     <tr>
        <td><strong>Device lock</strong></td>
        <td>Ability to Lock the device via Entgra IoT Server.
        </td>
     </tr>
     <tr>
        <td><strong>Location</strong></td>
        <td>Ability to request coordinates of device location via Entgra IoT Server.
        This configuration will be applied only on iOS devices.</td>
     </tr>
     <tr>
        <td><strong>Ring</strong></td>
        <td>Ability to Ring the device via Entgra IoT Server.
        This configuration will be applied only on iOS devices.</td>
     </tr>
     <tr>
        <td><strong>Clear Passcode</strong></td>
        <td>Ability to Clear current passcode via Entgra IoT Server.
        This configuration will be applied only on iOS devices.
        </td>
     </tr>
        <td><strong>Enterprise Wipe/strong></td>
        <td>Ability to Remove enterprise applications via Entgra IoT Server.
        </td>
     </tr>
     <tr>
         <td><strong>Send notification</strong></td>
         <td>Ability to Send notification operation via Entgra IoT Server.
         This configuration will be applied only on iOS devices.
         </td>
     </tr>
         <td><strong> Wipe data/strong></td>
         <td>Ability to Factory reset the device via Entgra IoT Server.
         </td>
     </tr>
     <tr>
        <td><strong>Voice roaming</strong></td>
        <td>Ability to Enable or disable voice roaming via Entgra IoT Server.</td>
     </tr>  
     <tr>
      <td><strong>Data roaming</strong></td>
      <td>Ability to enable or disable data roaming via Entgra IoT Server.
      </td>
     </tr>
     <tr>
      <td><strong>Bluetooth</strong></td>
      <td>Ability to enable or disable bluetooth via Entgra IoT Server.</td>
     </tr>
     <tr>
      <td><strong>Personal hotspot</strong></td>
      <td>Ability to enable or disable personal hotspot via Entgra IoT Server.</td>
     </tr>
     <tr>
      <td><strong>Wallpaper</strong></td>
      <td>Ability to set the wall paper of home screen and lock screen of device via Entgra IoT 
      Server. Upload the image that you want to set as wall paper. <img src="../../../../image/60038.png" style="border:5px solid black">
      Select number related to the all paper location.<br>
      1. Lock screen<br>
      2. Home screen<br>
      3. Lock and Home screens
      </td>
     </tr>
      <td><strong>Application configurations</strong></td>
      <td>Ability to manage the app configurations of third party managed applications. 
      Enter the application identifire.<br> <img src="../../../../image/60039.png"style="border:5px solid black"><br>
      Enter the restriction payload. (It is optional)<br><img src="../../../../image/60040.png" style="border:5px solid black">
      </td>
     </tr>
     <tr>
      <td><strong>Device shutdown</strong></td>
      <td>Ability to shut down device via Entgra IoT Server.
      This configuration will be applied only on macOS devices.</td>
     </tr>  
     <tr>
       <td><strong>Device restart</strong></td>
       <td>Ability to restart device via Entgra IoT Server.
       This configuration will be applied only on macOS devices.</td>
     </tr>  
  </tbody>
</table>
---
bookCollapseSection: true
weight: 2
---
# Managing Enrolled Devices

{{< hint info >}}
   <strong>Pre-requisites</strong>
   <br>
   <ul style="list-style-type:disc;">
       <li>The server has to be <a href="{{< param doclink >}}product-guide/download-and-start-the-server/">downloaded and started</a></li>
       <li>Must have been logged on to the server's <a href="{{< param doclink >}}product-guide/login-guide/">Device Management Portal.</a></li>
   </ul>
   {{< /   hint >}}

## View an Enrolled Device

1.Click **View** under Devices.

<img src = "../../../image/60004.png" style="border:5px solid black ">

2.You will be directed to the page with the enrolled devices.

<img src = "../../../image/60005.png" style="border:5px solid black ">

3.Click on the Mac OS device you enrolled. You will be directed to the device details page.

<img src = "../../../image/60006.png" style="border:5px solid black ">


## Grouping Devices

**In this tutorial**, you add your enrolled device/devices to a group. Grouping allows you to monitor and view device data of many devices in one go. This way you can view any abnormal behavior of the devices at a glance and take necessary actions to prevent it.

For example, sharing the devices in your lobby with your technician. For this purpose, you can create a group named Lobby, add the devices in the lobby to the created group, and share the group with the technician. Now the technician is able to view the devices in the group, analyze the data gathered by the devices and come to a conclusion on the devices that need attention.





By default, Entgra IoT Server has a group created named Bring Your Own Device (BYOD). It groups all the devices that are owned by device users.





Let's get started!

* * *

### Add a Device Group

Start off by creating a new device group in Entgra IoT Server:

1.  Click **Add** under GROUPS to add a new group. 

     <img src = "../../../image/352819017.png" style="border:5px solid black ">

2.  Provide the required details and click **Add.**

    *   **Group Name**: Type the group name.

    *   **Description**: Type a short description for the group.
    
        <img src = "../../../image/352819011.png" style="border:5px solid black ">


### Add Devices to the Group

Follow the steps given below to add the device/s to the group that you created:

1.  Click **View** under GROUPS.

    <img src = "../../../image/352818963.png" style="border:5px solid black "> 
    
2.  Click on the group you want to add the devices to.

3.  Click **Assign from My Devices**.

    <img src = "../../../image/352819005.png" style="border:5px solid black "> 
    
4.  Click **Select** to select the devices you need to add to the group and click **Add To Group**.

5.  Select the group from the drop-down list and click **Add device to group**. 

    <img src = "../../../image/352818999.png" style="border:5px solid black ">

The groups that appear in the drop down list are the groups you created through [add a device group](#add-a-device-group).

To confirm that the devices were added to the group, go to the group management page, click on the device group, and check if the devices were added.

### Sharing Groups with User Roles

You need to share the device group you created with other users so that they too can access the device type. 

Follows the steps given below:

1.  <a href="{{< param doclink >}}product-guide/download-and-start-the-server/">Download and start the server.</a>
    
2.  Log on to the server's<a href="{{< param doclink >}}product-guide/login-guide/"> Device Management Portal.</a>

3.  Click **View** under GROUPS. 
 
 <img src = "../../../image/352818981.png" style="border:5px solid black ">

4.  Click the share icon on the group you want to share.

<img src = "../../../image/352818993.png" style="border:5px solid black "> 

5.  Share the groups with user roles:  

    *   Share the group with existing user roles by entering the role name or names and clicking **Share**.
    *   Do you want to create a new role and share with the group? Click **New Role** and [create the new role]({{< param doclink >}}product-guide/manage-roles/#adding-a-role-and-permissions). Next, navigate to the group management page, click **Share**, enter the name of the role you just created, and click **Share**.

    *   Do you want to share the group with more roles but want them to be merged as a new role? Enter the names of the roles you want to share with the group, click **New Role from Selection**, and enter the name of the new role you are creating. Click **Yes** if you want to [add new users to the role]({{< param doclink >}}product-guide/manage-users/) you just created or click **No** to finish sharing the group.

        The permissions of the roles you selected will be merged to create the permissions for the new role.

        ![image](../../../image/352818975.png)
6.  If you need to add more user/s to the selected roles, select **Yes** in the confirmation message that appears and you are directed to the [User Management]({{< param doclink >}}product-guide/manage-users/) screen. Else select **No** and you are done with sharing the group with the selected user roles. 


### Update Group Details

Follow the steps given below, to update the group name or description.

1.Click **View** under GROUPS.

<img src = "../../../image/352818963.png" style="border:5px solid black "> 

2.Click Edit in your device group.

<img src = "../../../image/60008.png" style="border:5px solid black ">

3.Update the details and click **Update**.  

<img src = "../../../image/352818969.png" style="border:5px solid black ">


### Remove a Group

Follow the steps given below, to delete a group from Entgra IoT Server.

1.  Click **View** under GROUPS. 

    <img src = "../../../image/352818963.png" style="border:5px solid black ">
    
2.  Select the group to be removed, and click **Delete** under the particular group.

    <img src = "../../../image/60007.png" style="border:5px solid black ">

3.  Click **Yes**, to confirm that you want to delete the group.  

    <img src = "../../../image/352818957.png" style="border:5px solid black ">
    
    
## Monitoring the Device Status
        
Due to various reasons, the devices registered with Entgra IoT Server might not be able to communicate with the server continuously. When the device is not actively communicating with the server, you need to know about it to be able take necessary actions, such as checking if the device has any malfunctions and repairing it or checking if the device was stolen.
To get a clear understanding, let's look at how this works in the Entgra IoT Server.
    
<strong>If the device and the server are actively communicating, the device is shown as Active.</strong>
    
In the device management console, click **View** under Devices. You can see all your registered devices and their device statuses.

<img src = "../../../image/352821749.png" style="border:5px solid black ">
     
<strong>If the server is unable to communicate with the device within a defined time period, the device is shown as Unreachable.</strong>
    
In the device management console, click **View** under Devices. You can see all your registered devices and their device statuses.

<img src = "../../../image/352821760.png" style="border:5px solid black ">
      
<strong>If the server is still unable to communicate with the device after a defined time period, the device is shown as Inactive.</strong>
    
In the device management console, click **View** under Devices. You can see all your registered devices and their device statuses.

<img src = "../../../image/352821743.png" style="border:5px solid black ">
    
  
<strong>If the device starts to communicate with the device after some time, the device status is updated back to Active.</strong>
The device's status has the following lifecycle:

<img src = "../../../image/352821755.png" style="border:5px solid black ">
    
The device monitoring task is not applicable for all IoT devices. Therefore, you can choose to enable or disable it for your device type. Let's take a look at how you can configure Entgra IoT Server and your device type to monitor the device status.
    
1.  Open the `<IOTS_HOME>/conf/cdm-config.xml` file and make sure the `DeviceStatusTaskConfig` is enabled. This configuration is enabled by default.
    
    If the `DeviceStatusTaskConfig` is enabled (or enabled on a node that is in a clustered setup) it will run the status monitoring task in the server. If the configuration is disabled, the server will not monitor the status of the devices.
    
    <DeviceStatusTaskConfig>
        <Enable>true</Enable>
    </DeviceStatusTaskConfig>
        
    
2.  Configure the device type to go into the unreachable state and then to the inactive state after a specified time.  
        Navigate to the `<IOTS_HOME>/repository/deployment/server/devicetype` directory, open the `<DEVICE_TYPE>.xml` file, and configure the fields given below:  
        The default configuration in the `android.xml` file is shown below:

    <DeviceStatusTaskConfig>
         <RequireStatusMonitoring>true</RequireStatusMonitoring>
         <Frequency>300</Frequency>
         <IdleTimeToMarkUnreachable>300</IdleTimeToMarkUnreachable>
         <IdleTimeToMarkInactive>600</IdleTimeToMarkInactive>
    </DeviceStatusTaskConfig>
       
    
<table>
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th>
            </th>
            <td>
                <p>If the value is set to true, it enables the status monitoring task for the device type. Else it will not monitor the status of the device type.</p>
                <p>For the task to run on the specified device type it must be <a href="#monitoring-the-device-status">enabled on the server as shown in step 1 above</a>.</p>
            </td>
        </tr>
        <tr>
            <th>
            </th>
            <td>Define how often this task should run for the specified device type. The value needs to be given in seconds.
                <p>Make sure to have the value above 60 seconds.</p>
            </td>
        </tr>
        <tr>
            <th>
            </th>
            <td>Define after how long the device needs to be marked as unreachable. The value needs to be given in seconds.</td>
        </tr>
        <tr>
            <th>
            </th>

git        </tr>
    </tbody>
</table>        
      
In addition to the above configurations, for the device monitoring task to actively function, you need to have pending operations on the device end. When there are pending operations the device communicates with the server to send the operation details to the server and through it, the server keeps track and ensures that the device is active.
    
    
    



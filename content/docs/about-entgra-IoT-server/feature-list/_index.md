---
bookCollapseSection: true
weight: 4
---
# Feature List

Entgra EMM server comes with a variety of features optimally catering for every enterprise mobility need that you are likely to encounter in your enterprise.  

Accredited as a Google Enterprise EMM Partner, for Android devices the available features of our EMM server include forced confinement to specific WiFi networks, remote keyboard/mouse inputs on screen-share mode, pre-approved connected peripheral plug-in and disabling airplane mode restrictions.

The reporting functionality of the server extends to reports for tracking devices that do not have a mandatory application installed, changed SIM details and reports on enrolled as well as unenrolled devices. 
 
For Windows devices, remote locking and clear passcode features are available in case of loss or theft of the device. 

Bulk device enrollment and auto-assignment to groups is facilitated for all device types with the option of selecting pre-approved serial numbers and/or any other property.  
 
Our support for Android, iOS & macOS and  Windows operating systems enables wider reach over a range of platforms. 

For the available features in each of the operating systems, click on the relevant OS below:

<div align="center">

<table align="center" class="tableizer-table" width="100%">
<thead bgcolor="#B8C4CC">
<tr class="tableizer-firstrow"><th><font style="Tahoma" size="2"><a href="android">Android</a></font></th>
<th><font style="Arial" size="2"><a href="ios">iOS & macOS</a></font></th>
<th><font style="Arial" size="2"><a href="windows">Windows</a></font></th>
</tr></thead>
<tbody aria-autocomplete="both">
<tr>
<td align="center" width="25%"><a href="android">
<img src = "../../image/logo_android_website.jpg" style="border:1px solid black "></a>
</td>
<td align="center" width="25%"><a href="ios">
<img src = "../../image/logo_iOS_website.jpg" style="border:1px solid black "></a>
</td>
<td align="center" width="25%"><a href="windows">
<img src = "../../image/logo_windows_website.jpg" style="border:1px solid black "></a>
</td>
</tr></tbody></table>
</div>
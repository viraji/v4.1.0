---
bookCollapseSection: true
weight: 1
---

# Android Features

Entgra IoTS 4.0.0 is a <a target=_blank href="https://androidenterprisepartners.withgoogle.com/provider/#!/nO0FRKVachIRfVcd1gby">Google Enterprise Partner for Android</a>.  

## Supported Operations

Entgra IoT Server facilitates one time <a target="_blank" href="{{< param doclink>}}key-concepts/operations-and-policies/">Operations</a> that can be performed remotely via the Device Management Console. These operations are useful for runtime maintenance of devices.

The type of operations available for Android devices and are applicable for each enrollment type is summed up as per the table below. For details on each, click <a href="../../../product-guide/device-management-guide/android-devices/android-device-operations/">here</a>. 


<table border="1" class="tableizer-table">
<thead bgcolor="#9bc9f1"><tr class="tableizer-firstrow"><th><font style="Arial" size="2"> Feature - Description</font></th><th><font style="Arial" size="2">Legacy</font></th><th><font style="Arial" size="2">Work Profile</font></th></th><th><font style="Arial" size="2">Dedicated (Kiosk)</font></th></th><th><font style="Arial" size="2">Fully Managed (COPE)</font></th><th><font style="Arial" size="2">System App</font></th></tr></thead><tbody>
 
<tr><td><font style="Tahoma" size="2">Remote screen keyboard and mouse inputs in screen-sharing mode.</font></td></td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
<tr><td><font style="Tahoma" size="2">Allow-list/Block-list connected peripherals plugged-in.</font></td></td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
<tr><td><font style="Tahoma" size="2">Force devices to be locked on to a given WiFi network.</font></td></td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
<tr><td><font style="Tahoma" size="2">Entgra secure browser application with remote settings.</font></td></td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
<tr><td><font style="Tahoma" size="2">Device location history view to track the fleet’s history.</font></td></td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
<tr><td><font style="Tahoma" size="2">Lock device when passcode fail attempts exceeded the limit.</font></td></td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
<tr><td><font style="Tahoma" size="2">Notification displaying for Kiosk mode.</font></td></td><td align="center"><font style="Tahoma" size="2">X</td><td align="center"><font style="Tahoma" size="2">X</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">X</td><td align="center"><font style="Tahoma" size="2">X</td></tr>
<tr><td><font style="Tahoma" size="2">Display a custom message when the device is locked and for locked settings.</font></td></td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
<tr><td><font style="Tahoma" size="2">Offline un-enrollement via a special per device admin pin code.</font></td></td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
<tr><td><font style="Tahoma" size="2">Disable airplane mode restrictions.</font></td></td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
<tr><td><font style="Tahoma" size="2">Enroll COPE device using mobile data after a factory reset.</font></td></td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Get Device Information  - Fetch the device's runtime information.</font></td></td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Get Device Location Information - Fetch the device's current location. </td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Get Installed Applications - Fetch the device's installed application list. </td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Ring Device - Ring the device for the purpose of locating the device in case of misplacement.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Upload Device - Upload file to a specific folder on the device. </td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Download Device - Download file on a specific folder on the device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Mute Device - Put the device in silent mode.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Change Lock Code - Changes the device's currently set lock code. From Android N upwards, clear passcode will not work.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Clear Password - Remove any password that the device owner has put. From Android N upwards, clear passcode will not work.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Send Notifications/Messages - Send a notification (message) to the device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Enterprise Wipe - Wipe the entreprise portion of the device. </td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Wipe Device (Factory reset) - Factory reset a device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Device Lock (soft lock) - Lock the device remotely. Similar to pressing the power button on the device and locking it.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Reboot Device - Restart the phone for example for troubleshooting purposes.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Upgrade Firmware - Upgrade Android operating firmware ensuring that firmware and the device has to be compatible and only applicable in OEM scenarios.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Execute Shell Command - Remotely execute the shell commands on the device's command prompt.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Hard Lock - Lock a device remotely by an admin and only the admin can unlock the device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">&nbsp;</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Manage Web Clip - Install a shortcut link to a web page/web app on the phone's home screen.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Trigger Google Play App - Install an app from the google play store.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Install/Uninstall/update applications - Capability to perform various application management tasks such as install, uninstall and update apps.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">View Device Screen - Screen sharing with the Admin.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Remote Control Device - Allow Admin to remotely control the device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">&nbsp;</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Get Logcat - View the log of the operating system.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Silent App Install - Install apps on the device without prompting the user to click install.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Remote Kiosk Enable - Enable or disable kiosk mode remotely for maintenance reasons, troubleshooting etc.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
</tbody></table>

## Policies

The <a target="_blank" href="{{< param doclink>}}key-concepts/operations-and-policies/">Policies</a> that can be applied on an Android device depends on the way the device is enrolled with the server. 

Accordingly, the table below indicates the policies applicable for each type of enrollment. 

<table class="tableizer-table" border="1">
    <thead bgcolor="#9bc9f1">
        <tr class="tableizer-firstrow">
            <th><font style="Tahoma" size="2">Feature - Description</th>
            <th><font style="Tahoma" size="2">Legacy</th>
            <th><font style="Tahoma" size="2">Work Profile</th>
            <th><font style="Tahoma" size="2">Dedicated / Fully Managed</th>
            <th><font style="Tahoma" size="2">Fully Managed (COPE)</th>
            <th><font style="Tahoma" size="2">System App</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2"><a href="../../../product-guide/device-management-guide/android-devices/android-device-policies/passcode-policy/">Passcode Policy - Add a passcode strength policy to the device or to work profile</a></td>
                    <td align="center"><font style="Tahoma" size="2">✓</td>
                    <td align="center"><font style="Tahoma" size="2">✓</td>
                    <td align="center"><font style="Tahoma" size="2">✓</td>
                    <td align="center"><font style="Tahoma" size="2">✓</td>
                    <td align="center"><font style="Tahoma" size="2">✓</td>
        </tr>
              <tr>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2"><a href="../../../product-guide/device-management-guide/android-devices/android-device-policies/encryption-settings/">Encryption Settings - Execute the encypt device storage.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2"> <a href="../../../product-guide/device-management-guide/android-devices/android-device-policies/wi-fi-settings/">Wi-Fi Settings - Push a configuration contaning the wifi profile of the company.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2"><a href="../../../product-guide/device-management-guide/android-devices/android-device-policies/virtual-private-network/">Virtual Private network (VPN Settings) - Push a configuration contaning the VPN profile of the company.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2"><a href="../../../product-guide/device-management-guide/android-devices/android-device-policies/work-profile-configurations/">Work-Profile Configurations - Decides which system apps must be enabled or disabled in a work profile</a></td>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="../../../product-guide/device-management-guide/android-devices/android-device-policies/c-o-s-u-profile-configuration/">COSU Profile Configuration - Configure the behaviour of the Kiosk</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="../../../product-guide/device-management-guide/android-devices/android-device-policies/application-restriction-settings/">Application Restriction Settings - Decides which apps are allowed do be in a device.</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
        </tr>
        <tr>
                    <td><font style="Tahoma" size="2">App screen usage time policy to track/restrict screen usage per app.</td>
                    <td align="center"><font style="Tahoma" size="2">✓</td>
                    <td align="center"><font style="Tahoma" size="2">✓</td>
                    <td align="center"><font style="Tahoma" size="2">✓</td>
                    <td align="center"><font style="Tahoma" size="2">✓</td>
                    <td align="center"><font style="Tahoma" size="2">✓</td>
                </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="../../../product-guide/device-management-guide/android-devices/android-device-policies/runtime-permission-policy/">Runtime permissions - Permissions app require to work can be uptomatically granted and locked.</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="../../../product-guide/device-management-guide/android-devices/android-device-policies/system-update-policy/">System Update Policy (COSU) - Specify the strategy or the time windows to perform OS updates.</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><!--a href="../../../product-guide/device-management-guide/android-devices/android-device-policies/system-update-policy/"-->Monitor/Revoke Policies - Continuously monitor the policies of the device to detect any policy violations.<!--/a--></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="../../../product-guide/device-management-guide/android-devices/android-device-policies/certificate-install/">Certificate Install Settings - Install certificate to devices remotely.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="../../../product-guide/device-management-guide/android-devices/android-device-policies/global-proxy-settings/">Global Proxy settings - Reroute all the http communication of a device via a global http proxy.</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="../../../product-guide/device-management-guide/android-devices/android-device-policies/enrollment-application-install/">Enrollment app install - Decides which apps needs to be installed upon enrollment.</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><!--a href="../../../product-guide/device-management-guide/android-devices/android-device-policies/system-update-policy/"-->Remote App configurations - Send the app configurations for user's installed apps.</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><!--a href="../../../product-guide/device-management-guide/android-devices/android-device-policies/system-update-policy/"-->Disable removal of profile - Disable the user's ability to unenroll from EMM.</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2"></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
        </tr>
    </tbody>
</table>

###  Restrictions Policy 

<a target="_blank" href="../../../product-guide/device-management-guide/android-device/android-device-policies/restrictions/">Restriction Policies</a>  are those that can be applied on a device restricting or controlling the use of certain specific device features. There are a large number of restrictions that can be applied on an Android device. 

The following table lists the available Restriction Policies for Android devices. 


<table class="tableizer-table" border="1">
<thead bgcolor="#9bc9f1"><tr class="tableizer-firstrow"><th width="50%"><font style="Tahoma" size="2">Feature - Description</th><th width="10%"><font style="Tahoma" size="2">Legacy</th><th width="10%"><font style="Tahoma" size="2">Work Profile</th><th width="10%"><font style="Tahoma" size="2">Dedicated / Fully Managed / System</th><th width="10%"><font style="Tahoma" size="2">Fully managed (COPE)</th><th width="10%"><font style="Tahoma" size="2">System app</th></tr></thead><tbody>
 <tr><td><font style="Tahoma" size="2">Disable access to camera</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable modifying certificates in the device</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable configuring VPN settings</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable configuring App control by hiding the status bar of App Control</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable cross-profile copy-paste - Copying text between profiles is blocked.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable debugging - Disable usb debuging</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable installing apps to the device</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable installing apps from unknown sources</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable modifying accounts such as Google, Facebook from being modified/ added/ removed</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable outgoing beams - Disable using NFC to transfer data.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable sharing device location.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable uninstalling apps</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable parent profile app linking - Disable apps in the personal profile to handle web links from the work profile.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Ensure verifying apps - Enforce only verified apps can be installed on the device.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable screen capture - Disable capturing the screen of the device.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Enable auto timing - Enable or diable using time from mobile network as system time.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable  SMS - Disable access to SMS.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable volume adjust - Disable adjusting the volume of the device.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable cell broadcast - Disables cell broadcasting messages of the network.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable configuring bluetooth settings.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable configuring moble network settings.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable configuring tethering settings.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable configuring WiFi settings.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable  safe boot - Disable booting into safe mode.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable outgoing calls</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable mount physical media - Disable plugging into different media devices.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable create window - Disable showing certain notifications, toasts and alert by apps.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable factory resetting of devices.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable removing users from device.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable adding new users to device.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable network reset - Disable user from performing network setting reset.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable USB file transfer - Disable transfering data over USB.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable unmute microphone - Configure access to microphone.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable status bar - Block user from opening the notification bar and access to status bar.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable set wallpaper - Disable changing wallpapers.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable auto fill - Disable auto filling forms.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable bluetooth - Disable bluetooth.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable bluetooth sharing - Disable sharing via bluetooth.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable data roaming - Disable data roaming. </td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
</tbody></table>


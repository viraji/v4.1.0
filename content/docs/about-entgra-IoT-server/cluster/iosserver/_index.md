---
bookCollapseSection: true
weight: 7
title: "Clustering the iOS Server"
---

# Clustering the iOS Server

Let's take a look at the steps you need to follow to cluster WSO2 IoT Server with iOS. This section is not required if you don't want to enroll and manage iOS devices.

<strong>Note:</strong> Before you begin, make sure to install the <a target="_blank" href="https://docs.wso2.com/display/IoTS310/Installing+iOS+Features">iOS features</a>.

Follow the steps given below:
<ol>
<li>Run the following scripts in the given order to create the CA, RA and SSL certificates.
Make sure to create the <I>output</I> folder and use the NGINX private key <I>(iots310_wso2_com.key)</I> as the CA.
<br><strong>Note:</strong> Run the scripts only on one server and copy them to the other servers.
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
SSL_PASS="wso2carbon"
CA_SUBJ="/C=SL/ST=Western/L=Colombo/O=WSO2/OU=CDM/CN=*.iots310.wso2.com/EMAILADDRESS=noreply@wso2.com"
RA_SUBJ="/C=SL/ST=Western/L=Colombo/O=WSO2/OU=CDM/CN=iots310.wso2.com/EMAILADDRESS=noreply@wso2.com"
SSL_SUBJ="/C=SL/ST=Western/L=Colombo/O=WSO2/OU=CDM/CN="$1
 
------------------------------------------------------------------------------------------
echo "Generating CA"
openssl req -new -key ./output/iots310_wso2_com.key -out ./output/ca.csr -subj $CA_SUBJ
openssl x509 -req -days 365 -in ./output/ca.csr -signkey ./output/iots310_wso2_com.key -out ./output/ca.crt -extensions v3_ca -extfile ./needed_files/openssl.cnf
openssl rsa -in ./output/iots310_wso2_com.key -text > ./output/ca_private.pem
openssl x509 -in ./output/ca.crt -out ./output/ca_cert.pem
 
------------------------------------------------------------------------------------------
echo "Generating RA"
openssl genrsa -out ./output/ra_private.key 4096
openssl req -new -key ./output/ra_private.key -out ./output/ra.csr -subj $RA_SUBJ
openssl x509 -req -days 365 -in ./output/ra.csr -CA ./output/ca.crt -CAkey ./output/iots310_wso2_com.key -set_serial 12132121241241 -out ./output/ra.crt -extensions v3_req -extfile ./needed_files/openssl.cnf
openssl rsa -in ./output/ra_private.key -text > ./output/ra_private.pem
openssl x509 -in ./output/ra.crt -out ./output/ra_cert.pem
 
echo "Generating SSL"
openssl genrsa -out ./output/ia.key 4096
openssl req -new -key ./output/ia.key -out ./output/ia.csr  -subj $SSL_SUBJ
openssl x509 -req -days 730 -in ./output/ia.csr -CA ./output/ca_cert.pem -CAkey ./output/ca_private.pem -set_serial 34467867966445 -out ./output/ia.crt
 
------------------------------------------------------------------------------------------
echo "Export to PKCS12"
openssl pkcs12 -export -out ./output/KEYSTORE.p12 -inkey ./output/ia.key -in ./output/ia.crt -CAfile ./output/ca_cert.pem -name "ioscluster" -password pass:$SSL_PASS
openssl pkcs12 -export -out ./output/ca.p12 -inkey ./output/ca_private.pem -in ./output/ca_cert.pem -name "cacert" -password pass:$SSL_PASS
openssl pkcs12 -export -out ./output/ra.p12 -inkey ./output/ra_private.pem -in ./output/ra_cert.pem -chain -CAfile ./output/ca_cert.pem -name "racert" -password pass:$SSL_PASS
 
------------------------------------------------------------------------------------------
echo "Export PKCS12 to JKS"
keytool -importkeystore -srckeystore ./output/KEYSTORE.p12 -srcstoretype PKCS12 -destkeystore ./output/wso2carbon.jks -deststorepass wso2carbon -srcstorepass wso2carbon -noprompt
keytool -importkeystore -srckeystore ./output/KEYSTORE.p12 -srcstoretype PKCS12 -destkeystore ./output/client-truststore.jks -deststorepass wso2carbon -srcstorepass wso2carbon -noprompt
keytool -importkeystore -srckeystore ./output/ca.p12 -srcstoretype PKCS12 -destkeystore ./output/wso2certs.jks -deststorepass wso2carbon -srcstorepass wso2carbon -noprompt
keytool -importkeystore -srckeystore ./output/ra.p12 -srcstoretype PKCS12 -destkeystore ./output/wso2certs.jks -deststorepass wso2carbon -srcstorepass wso2carbon -noprompt
Configure the <IOTS_HOME>/conf/iot-api-config.xml file in the manager node as shown below:

<VerificationEndpoint>https://iots310.wso2.com/api/certificate-mgt/v1.0/admin/certificates/verify/</VerificationEndpoint>
<DynamicClientRegistrationEndpoint>https://keymgt.iots310.wso2.com/client-registration/v0.11/register</DynamicClientRegistrationEndpoint>
<OauthTokenEndpoint>https://gateway.iots310.wso2.com/token</OauthTokenEndpoint>
</pre>
</code>
</font></td></tr></table>
</li>
<li>Configure the <I>&lt;IOTS_HOME&gt;/conf/iot-api-config.xml</I> file in the manager node as shown below:
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
&lt;VerificationEndpoint&gt;https://iots310.wso2.com/api/certificate-mgt/v1.0/admin/certificates/verify/&lt;/VerificationEndpoint&gt;
&lt;DynamicClientRegistrationEndpoint&gt;https://keymgt.iots310.wso2.com/client-registration/v0.11/register&lt;/DynamicClientRegistrationEndpoint&gt;
&lt;OauthTokenEndpoint&gt;https://gateway.iots310.wso2.com/token&lt;/OauthTokenEndpoint&gt;
</pre>
</code>
</font></td></tr></table>
</li>
<li>Configure the <I>&lt;IOTS_HOME&gt;/conf/certificate-config.xml</I> file as shown below: 
<br><strong>Note:</strong> 
<I>wso2carbon</I> was used as the password when generating the CA and RA certificates using the scripts given in step 1. If you used a different password, make sure to update the properties given below accordingly.
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
&lt;CAPrivateKeyPassword&gt;wso2carbon&lt;/CAPrivateKeyPassword&gt;
&lt;RAPrivateKeyPassword&gt;wso2carbon&lt;/RAPrivateKeyPassword&gt;
</font></td></tr></table>
</li>
<li>Disable the task server in the <I>&lt;IOTS_HOME&gt;/repository/deployment/server/devicetypes/ios.xml</I> file on the worker node.
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
&lt;TaskConfiguration&gt;
    &lt;Enable>false&lt;/Enable&gt;
    ......
&lt;/TaskConfiguration&gt;
</pre>
</code>
</font></td></tr></table>
</li>
<li>Configure the following properties in the <I>&lt;IOTS_HOME&gt;/repository/deployment/server/jaggeryapps/ios-web-agent/app/conf/config.json</I> file as shown below.
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
"httpsURL": "https://mgt.iots310.wso2.com",
"httpURL": "http://mgt.iots310.wso2.com",
"tokenServiceURL": "https://gateway.iots310.wso2.com/token"
"location": "https://mgt.iots310.wso2.com/ios-web-agent/public/mdm.page.enrollments.ios.download-agent/asset/ios-agent.ipa",
</pre>
</code>
</font></td></tr></table>
</li>
<li>Start the manager node, Sign in to the device management console, and navigate to the platform configurations section to configure the iOS configurations.
<ul style="list-style-type:circle;">
<li>Make use you have the MDM certificate and the MDM APNS certificate before you configure the iOS configurations. For more information, see <a target="_blank" href="https://docs.wso2.com/display/IoTS310/Generating+Certificates+from+the+Apple+Developer+Portal">Generating Certificates from the Apple Developer Portal</a>.</li>
<li>Configure the iOS platform settings. For more information, see <a target="_blank" href="https://docs.wso2.com/display/IoTS310/iOS+Platform+Configurations">iOS Platform Configurations</a>.</li>
</ul></li>
</ol>
You can now start enrolling iOS devices with the IoT Server.